import Spinner from 'react-bootstrap/Spinner';

function GrowSpinner() {
  return (
    <div className='text-center'>
        <Spinner animation="grow" />
    </div>
  )
}

export default GrowSpinner;