import axios from 'axios';
import { useState, useEffect } from 'react';
import Table from 'react-bootstrap/Table';
import GrowSpinner from './GrowSpinner';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

type Dmbs = {
  id: number,
  nombre: string,
  lanzamiento: number,
  desarrollador: string
}

function DataTable() {
  const [data, setData] = useState<Dmbs[]>([]);
  const [spinner, setSpinner] = useState(true);

  const [showAdd, setShowAdd] = useState(false);
  const handleClose = () => setShowAdd(false);
  const handleShow = () => setShowAdd(true);

  const [showDelete, setShowDelete] = useState(false);
  const handleCloseDelete = () => setShowDelete(false);
  const handleShowDelete = () => setShowDelete(true);

  const [showEdit, setShowEdit] = useState(false);
  const handleCloseEdit = () => setShowEdit(false);
  const handleShowEdit = () => setShowEdit(true);

  const [dbms, setDbms] = useState<Dmbs>({
    id: 0,
    nombre: '',
    lanzamiento: 0,
    desarrollador: ''
  })

  const handleChange = (e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
  ) => {
    setDbms({
      ...dbms,
      [e.target.name]: e.target.value.trim(),
    });
  }


  const getData = async () => {
    await axios.get('https://api.c3rberus.dev/api/gestores').
      then(r => {
        console.log(r.data)
        setData(r.data);
      }).catch(error => {
        console.log(error)
      })
  }

  const postData = async () => {
    console.log(dbms)
    await axios.post('https://api.c3rberus.dev/api/gestores', dbms).then(response => {
      getData();
      handleClose();
    }).catch(error => {
      console.log(error)
    })
  }

  const selectDbms = (gestor: Dmbs, caso: string) => {
    setDbms(gestor);
    (caso === "Editar") ? handleShowEdit() : handleShowDelete();
  }



  useEffect(() => {
    getData()
    setSpinner(false);
  }, [])

  return (
    <>
      <Button variant="success" className='mb-2' onClick={handleShow}>Add New DBMS</Button>
      {spinner ? (<GrowSpinner />) : (
        <>
          <Table striped bordered hover responsive variant="dark">
            <thead>
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Release</th>
                <th>Developer</th>
                <th>Actions</th>

              </tr>
            </thead>
            <tbody>
              {data.map(gestor => (
                <tr key={gestor.id}>
                  <td>
                    {gestor.id}
                  </td>
                  <td>
                    {gestor.nombre}
                  </td>
                  <td>
                    {gestor.lanzamiento}
                  </td>
                  <td>
                    {gestor.desarrollador}
                  </td>
                  <td>
                    <Button variant="primary" className='my-2' onClick={handleShow}>Edit</Button> {' '}
                    <Button variant="danger" onClick={() => selectDbms(gestor, "Eliminar")}>Delete</Button>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
          <Modal show={showAdd} onHide={handleClose} backdrop={'static'} keyboard={false}>
            <Modal.Header closeButton>
              <Modal.Title>Insert on DBMS</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <div className='form-group'>
                <div className="mb-3">
                  <label>Name: </label>
                  <input type={"text"} name="nombre" required className={"form-control"} onChange={handleChange} />
                </div>
                <div className="mb-3">
                  <label>Release Date: </label>
                  <input type={"number"} name="lanzamiento" required className={"form-control"} onChange={handleChange} />
                </div>
                <div className="mb-3">
                  <label>Developer: </label>
                  <input type={"text"} name="desarrollador" required className={"form-control"} onChange={handleChange} />
                </div>
              </div>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose}>
                Close
              </Button>
              <Button variant="primary" onClick={postData}>
                Save Changes
              </Button>
            </Modal.Footer>
          </Modal>

          <Modal show={showDelete} onHide={handleCloseDelete} backdrop={'static'} keyboard={false}>
            <Modal.Header closeButton>
              <Modal.Title>Delete DBMS {dbms && dbms.nombre}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              Are you sure that want to delete this DBMS?
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={handleCloseDelete}>
                Cancel
              </Button>
              <Button variant="primary" >
                Delete
              </Button>
            </Modal.Footer>
          </Modal>
        </>
      )}
    </>
  );
}

export default DataTable;